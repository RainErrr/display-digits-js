module.exports = {
  testPathIgnorePatterns: ['/node_modules/', '/.idea/'],
  reporters: [
    'default',
    [
      'jest-junit',
      {
        suiteNameTemplate: '{filepath}',
        titleTemplate: '{title}',
        outputDirectory: 'build/test-results',
      },
    ],
  ],
}
