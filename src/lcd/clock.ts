import {templateSmall} from "./templates";
import {printer} from "./printer";

const clock = (): void => {
    const now = new Date()
    let seconds = now.getSeconds().toString().padStart(2, '0')
    let minutes = now.getMinutes().toString().padStart(2, '0')
    let hours = now.getHours().toString().padStart(2, '0')

    const html = document.querySelector('.clock')
    const time = hours + minutes + seconds

    if (html) {
        html.innerHTML = printer(time, templateSmall)
    }
}

setInterval(clock, 1000)

clock()