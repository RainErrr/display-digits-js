import {Template} from '../types/Template'

const printer = (digits: string, template: Template): string => {
    const digitsToArray: string[] = digits.split('')

    const result: string[] = new Array(template['1'].length).fill("")

    digitsToArray.forEach((_, i) => {
        result.forEach((_, j) => result[j] += template[digitsToArray[i]][j])
    })

    return result.join('\n')
}

export {printer}
