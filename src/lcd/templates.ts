import {Template} from "../types/Template";
import {Digit} from "../types/Digit";

export const templateSmall: Template = {
    [Digit.ONE]: ["   ", "  |", "  |"],
    [Digit.TWO]: [" _ ", " _|", "|_ "],
    [Digit.THREE]: [" _ ", " _|", " _|"],
    [Digit.FOUR]: ["   ", "|_|", "  |"],
    [Digit.FIVE]: [" _ ", "|_ ", " _|"],
    [Digit.SIX]: [" _ ", "|_ ", "|_|"],
    [Digit.SEVEN]: [" _ ", "  |", "  |"],
    [Digit.EIGHT]: [" _ ", "|_|", "|_|"],
    [Digit.NINE]: [" _ ", "|_|", " _|"],
    [Digit.ZERO]: [" _ ", "| |", "|_|"],
}

export const templateBig: Template = {
    [Digit.ONE]: ["    ", "   |", "   |", "   |", "   |"],
    [Digit.TWO]: [" __ ", "   |", " __|", "|   ", "|__ "],
    [Digit.THREE]: [" __ ", "   |", " __|", "   |", " __|"],
    [Digit.FOUR]: ["    ", "|  |", "|__|", "   |", "   |"],
    [Digit.FIVE]: [" __ ", "|   ", "|__ ", "   |", " __|"],
    [Digit.SIX]: [" __ ", "|   ", "|__ ", "|  |", "|__|"],
    [Digit.SEVEN]: [" __ ", "   |", "   |", "   |", "   |"],
    [Digit.EIGHT]: [" __ ", "|  |", "|__|", "|  |", "|__|"],
    [Digit.NINE]: [" __ ", "|  |", "|__|", "   |", " __|"],
    [Digit.ZERO]: [" __ ", "|  |", "|  |", "|  |", "|__|"],
}
