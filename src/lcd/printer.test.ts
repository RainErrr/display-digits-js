import {templateBig, templateSmall} from "./templates";
import {printer} from "./printer";

describe('lcd', () => {
    describe('small digits', () => {
        it('returns number one', () => {
            const expected =
                "   \n" +
                "  |\n" +
                "  |"
            expect(printer("1", templateSmall)).toEqual(expected)
        })

        it('returns number two', () => {
            const expected =
                " _ \n" +
                " _|\n" +
                "|_ "
            expect(printer("2", templateSmall)).toEqual(expected)
        })

        it('returns number three', () => {
            const expected =
                " _ \n" +
                " _|\n" +
                " _|"
            expect(printer("3", templateSmall)).toEqual(expected)
        })

        it('returns number four', () => {
            const expected =
                "   \n" +
                "|_|\n" +
                "  |"
            expect(printer("4", templateSmall)).toEqual(expected)
        })

        it('returns number five', () => {
            const expected =
                " _ \n" +
                "|_ \n" +
                " _|"
            expect(printer("5", templateSmall)).toEqual(expected)
        })

        it('returns number six', () => {
            const expected =
                " _ \n" +
                "|_ \n" +
                "|_|"
            expect(printer("6", templateSmall)).toEqual(expected)
        })

        it('returns number seven', () => {
            const expected =
                " _ \n" +
                "  |\n" +
                "  |"
            expect(printer("7", templateSmall)).toEqual(expected)
        })

        it('returns number eight', () => {
            const expected =
                " _ \n" +
                "|_|\n" +
                "|_|"
            expect(printer("8", templateSmall)).toEqual(expected)
        })

        it('returns number nine', () => {
            const expected =
                " _ \n" +
                "|_|\n" +
                " _|"
            expect(printer("9", templateSmall)).toEqual(expected)
        })

        it('returns number zero', () => {
            const expected =
                " _ \n" +
                "| |\n" +
                "|_|"
            expect(printer("0", templateSmall)).toEqual(expected)
        })

        it('returns number 123', () => {
            const expected =
                "    _  _ \n" +
                "  | _| _|\n" +
                "  ||_  _|"
            expect(printer("123", templateSmall)).toEqual(expected)
        })

        it('returns number 321', () => {
            const expected =
                " _  _    \n" +
                " _| _|  |\n" +
                " _||_   |"
            expect(printer("321", templateSmall)).toEqual(expected)
        })

        it('returns number 1234567890', () => {
            const expected =
                "    _  _     _  _  _  _  _  _ \n" +
                "  | _| _||_||_ |_   ||_||_|| |\n" +
                "  ||_  _|  | _||_|  ||_| _||_|"
            expect(printer("1234567890", templateSmall)).toEqual(expected)
        })

        it('returns number 9876543210', () => {
            const expected =
                " _  _  _  _  _     _  _     _ \n" +
                "|_||_|  ||_ |_ |_| _| _|  || |\n" +
                " _||_|  ||_| _|  | _||_   ||_|"
            expect(printer("9876543210", templateSmall)).toEqual(expected)
        })
    })

    describe('big digits', () => {
        it('returns number one', () => {
            const expected =
                "    \n" +
                "   |\n" +
                "   |\n" +
                "   |\n" +
                "   |"
            expect(printer("1", templateBig)).toEqual(expected)
        })

        it('returns number two', () => {
            const expected =
                " __ \n" +
                "   |\n" +
                " __|\n" +
                "|   \n" +
                "|__ "
            expect(printer("2", templateBig)).toEqual(expected)
        })

        it('returns number three', () => {
            const expected =
                " __ \n" +
                "   |\n" +
                " __|\n" +
                "   |\n" +
                " __|"
            expect(printer("3", templateBig)).toEqual(expected)
        })

        it('returns number four', () => {
            const expected =
                "    \n" +
                "|  |\n" +
                "|__|\n" +
                "   |\n" +
                "   |"
            expect(printer("4", templateBig)).toEqual(expected)
        })

        it('returns number five', () => {
            const expected =
                " __ \n" +
                "|   \n" +
                "|__ \n" +
                "   |\n" +
                " __|"
            expect(printer("5", templateBig)).toEqual(expected)
        })

        it('returns number six', () => {
            const expected =
                " __ \n" +
                "|   \n" +
                "|__ \n" +
                "|  |\n" +
                "|__|"
            expect(printer("6", templateBig)).toEqual(expected)
        })

        it('returns number seven', () => {
            const expected =
                " __ \n" +
                "   |\n" +
                "   |\n" +
                "   |\n" +
                "   |"
            expect(printer("7", templateBig)).toEqual(expected)
        })

        it('returns number eight', () => {
            const expected =
                " __ \n" +
                "|  |\n" +
                "|__|\n" +
                "|  |\n" +
                "|__|"
            expect(printer("8", templateBig)).toEqual(expected)
        })

        it('returns number nine', () => {
            const expected =
                " __ \n" +
                "|  |\n" +
                "|__|\n" +
                "   |\n" +
                " __|"
            expect(printer("9", templateBig)).toEqual(expected)
        })

        it('returns number zero', () => {
            const expected =
                " __ \n" +
                "|  |\n" +
                "|  |\n" +
                "|  |\n" +
                "|__|"
            expect(printer("0", templateBig)).toEqual(expected)
        })

        it('returns number 123', () => {
            const expected =
                "     __  __ \n" +
                "   |   |   |\n" +
                "   | __| __|\n" +
                "   ||      |\n" +
                "   ||__  __|"
            expect(printer("123", templateBig)).toEqual(expected)
        })

        it('returns number 321', () => {
            const expected =
                " __  __     \n" +
                "   |   |   |\n" +
                " __| __|   |\n" +
                "   ||      |\n" +
                " __||__    |"
            expect(printer("321", templateBig)).toEqual(expected)
        })

        it('returns number 1234567890', () => {
            const expected =
                "     __  __      __  __  __  __  __  __ \n" +
                "   |   |   ||  ||   |      ||  ||  ||  |\n" +
                "   | __| __||__||__ |__    ||__||__||  |\n" +
                "   ||      |   |   ||  |   ||  |   ||  |\n" +
                "   ||__  __|   | __||__|   ||__| __||__|"
            expect(printer("1234567890", templateBig)).toEqual(expected)
        })

        it('returns number 9876543210', () => {
            const expected =
                " __  __  __  __  __      __  __      __ \n" +
                "|  ||  |   ||   |   |  |   |   |   ||  |\n" +
                "|__||__|   ||__ |__ |__| __| __|   ||  |\n" +
                "   ||  |   ||  |   |   |   ||      ||  |\n" +
                " __||__|   ||__| __|   | __||__    ||__|"
            expect(printer("9876543210", templateBig)).toEqual(expected)
        })
    })
})
