import {Digit} from "./Digit";

export type Template = {
    [key in Digit]: string[]
}