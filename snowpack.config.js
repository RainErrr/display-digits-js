module.exports = {
  mount: {
    public: '/',
    src: '/_dist_'
  },
  exclude: [
    '**/node_modules/**/*',
    '**/*.test.ts'
  ],
  plugins: [
    '@snowpack/plugin-typescript'
  ],
  buildOptions: {
    out: 'dist'
  }
}
